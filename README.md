# CUCUMBER COM RUBY <br>

### Pré Requisitos
| <b>Recurso</b>       | <b>Versão</b>        |
| ------------- |:-------------:|
| Ruby          | ^2.5          |
| Bundler:      | ^2.0          |

### Instalação
Após realizar o clone do projeto, acessar o diretório do projeto e executar no terminal: <br>
```bash
bundler install
```
O bundler irá instalar todas as dependências do projeto

### Execução dos testes
Para executar os testes, basta chamar o cucumber no diretório do projeto:<br>
```bash
cucumber
```

### Relatórios
Para gerar relatórios padrões do Cucumber basta executar no diretório do projeto:<br>
```bash
cucumber features/specifications/reqres.feature --format html --out reports/reports.html
``` 
