class Reqres
  include HTTParty
  require_relative '/Invillia/estudos-ruby/HTTParty-cucumber/features/hooks/reqres_hook.rb'
  base_uri "https://reqres.in"

  def initialize(body)
    @options = {body => body}
    @options2 = {}
  end

  def postUser
    self.class.post("/api/users", @options)
  end

  def putUser (id)
    self.class.put("/api/users/#{id}", @options)
  end

  def getUser (id)
    self.class.get("/api/users/#{id}", @options2)
  end

  def deleteUser (id)
    self.class.delete("/api/users/#{id}", @options2)
  end

end