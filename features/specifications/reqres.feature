#language: pt
@reqres
Funcionalidade: Manter dados de Startup através de API
  Como um usuário do sistema
  Eu quero realizar as requisições na API
  A fim de manipular as informações do cadastro web do reqres

  Cenário: Consultar um Usuário
    Dado o endereço da API para manter o cadastro dos usuários do sistema
    Quando realizar uma requisição passando o ID 2 do Usuário
    Então a API irá retornar os dados do Usuário correspondente respondendo o código 200

  Cenário: Alterar um Usuário
    Dado o endereço da API para manter o cadastro dos usuários do sistema
    Quando realizar uma requisição para alterar um Usuário passando o ID 2
    Então a API irá retornar os dados do Usuário alterado respondendo o código 200

  Cenário: Deletar um Usuário
    Dado o endereço da API para manter o cadastro dos usuários do sistema
    Quando realizar uma requisição para deletar um Usuário passando o ID 2
    Então a API deverá responder com o código 204

  Cenário: Cadastrar um Usuário
    Dado o endereço da API para manter o cadastro dos usuários do sistema
    Quando realizar uma requisição para cadastrar um Usuário
    Então a API irá retornar os dados do cadastro do Usuário respondendo o código 201