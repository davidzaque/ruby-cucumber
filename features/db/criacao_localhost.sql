DROP TABLE IF EXISTS qa.log_teste;
DROP TABLE IF EXISTS qa.suite_caso_teste;
DROP TABLE IF EXISTS qa.suite_teste;
DROP TABLE IF EXISTS qa.caso_teste;

DROP SEQUENCE IF EXISTS qa.nu_seq_caso_teste;;
DROP SEQUENCE IF EXISTS qa.nu_seq_suite_teste;;
DROP SEQUENCE IF EXISTS qa.nu_seq_suite_caso_teste;;
DROP SEQUENCE IF EXISTS qa.nu_seq_log_teste;;

CREATE SEQUENCE IF NOT EXISTS qa.nu_seq_caso_teste;
CREATE SEQUENCE IF NOT EXISTS qa.nu_seq_suite_teste;
CREATE SEQUENCE IF NOT EXISTS qa.nu_seq_suite_caso_teste;
CREATE SEQUENCE IF NOT EXISTS qa.nu_seq_log_teste;

CREATE TABLE IF NOT EXISTS qa.caso_teste(
	nu_seq_caso_teste INT UNIQUE NOT NULL,
	descricao VARCHAR(255) UNIQUE NOT NULL,
	PRIMARY KEY (nu_seq_caso_teste)
	);

CREATE TABLE IF NOT EXISTS qa.suite_teste(
	nu_seq_suite_teste INT UNIQUE NOT NULL,
	descricao VARCHAR(255) UNIQUE NOT NULL,
	PRIMARY KEY (nu_seq_suite_teste)
	);

CREATE TABLE IF NOT EXISTS qa.suite_caso_teste(
    nu_seq_suite_caso_teste INT UNIQUE NOT NULL,
    nu_seq_suite_teste INT NOT NULL,
    nu_seq_caso_teste INT NOT NULL,
    FOREIGN KEY (nu_seq_suite_teste) REFERENCES qa.suite_teste (nu_seq_suite_teste),
    FOREIGN KEY (nu_seq_caso_teste) REFERENCES qa.caso_teste (nu_seq_caso_teste)
);

CREATE TABLE IF NOT EXISTS qa.log_teste
(
  nu_seq_log_teste integer NOT NULL DEFAULT nextval('qa.nu_seq_log_teste'::regclass),
  nu_seq_suite_caso_teste integer NOT NULL,
  situacao character varying(10) NOT NULL,
  response character varying NOT NULL,
  erro character varying(300),
  data_registro date NOT NULL,
  duracao_caso_teste character varying(30) NOT NULL,
  ambiente character varying(100) NOT NULL,
  user_name character varying(100) NOT NULL,
  user_ip character varying(20) NOT NULL,
  CONSTRAINT log_teste_nu_seq_suite_caso_teste_fkey FOREIGN KEY (nu_seq_suite_caso_teste)
      REFERENCES qa.suite_caso_teste (nu_seq_suite_caso_teste) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT log_teste_nu_seq_log_teste_key UNIQUE (nu_seq_log_teste)
);

ALTER SEQUENCE qa.nu_seq_caso_teste RESTART WITH 1;
ALTER SEQUENCE qa.nu_seq_suite_teste RESTART WITH 1;
ALTER SEQUENCE qa.nu_seq_suite_caso_teste RESTART WITH 1;
ALTER SEQUENCE qa.nu_seq_log_teste RESTART WITH 1;

ALTER TABLE qa.caso_teste ALTER COLUMN nu_seq_caso_teste SET DEFAULT NEXTVAL('qa.nu_seq_caso_teste');
ALTER TABLE qa.suite_teste ALTER COLUMN nu_seq_suite_teste SET DEFAULT NEXTVAL('qa.nu_seq_suite_teste');
ALTER TABLE qa.suite_caso_teste ALTER COLUMN nu_seq_suite_caso_teste SET DEFAULT NEXTVAL('qa.nu_seq_suite_caso_teste');
ALTER TABLE qa.log_teste ALTER COLUMN nu_seq_log_teste SET DEFAULT NEXTVAL('qa.nu_seq_log_teste');

SELECT * FROM qa.caso_teste;
SELECT * FROM qa.suite_teste ;
SELECT * FROM qa.suite_caso_teste ;
SELECT * FROM qa.log_teste;

SELECT qsct.nu_seq_suite_caso_teste, qst.descricao suite, qct.descricao caso_teste
FROM qa.suite_caso_teste qsct
JOIN qa.suite_teste qst on qsct.nu_seq_suite_teste = qst.nu_seq_suite_teste
JOIN qa.caso_teste qct on qsct.nu_seq_caso_teste = qct.nu_seq_caso_teste