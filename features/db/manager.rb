require('pg')
require_relative "../helper/helper.rb"

class Manager

  def initialize
    @host_addr = '127.0.0.1'
    @port = '5432'
    @database = 'david'
    @user = 'david'
    @pass = 'david'
  end

  def conn
    PG.connect(hostaddr: @host_addr, port: @port, dbname: @database,
               user: @user, password: @pass)
  end

  def query string
    res = conn.exec(string)
    conn&.close
    res
  end

  def find_suite suite_name
    query("SELECT nu_seq_suite_teste FROM qa.suite_teste WHERE descricao = '" +
              suite_name + "'")
  end

  def insert_suite description
    query("INSERT INTO qa.suite_teste VALUES (DEFAULT,'" + description + "');")
    find_suite description
  end

  def find_caso_teste test_case
    query("SELECT nu_seq_caso_teste FROM qa.caso_teste WHERE descricao = '" +
              test_case + "';")
  end

  def insert_caso_teste descricao
    query("INSERT INTO qa.caso_teste VALUES (DEFAULT,'" + descricao + "');")
    find_caso_teste descricao
  end

  def find_suite_caso_teste (nu_seq_suite_teste, nu_seq_caso_teste)
    query('SELECT nu_seq_suite_caso_teste FROM qa.suite_caso_teste qsct ' \
              'JOIN qa.suite_teste qst on qsct.nu_seq_suite_teste = qst.nu_seq_suite_teste ' \
              'JOIN qa.caso_teste qct ON qsct.nu_seq_caso_teste = qct.nu_seq_caso_teste ' \
              'WHERE qst.nu_seq_suite_teste = ' + nu_seq_suite_teste + ' ' \
              'AND qct.nu_seq_caso_teste = ' + nu_seq_caso_teste + ';')
  end

  def insert_suite_caso_teste (nu_seq_suite_teste, nu_seq_caso_teste)
    query('INSERT INTO qa.suite_caso_teste VALUES (DEFAULT,' + nu_seq_suite_teste + ', ' + nu_seq_caso_teste + ');')
    find_suite_caso_teste(nu_seq_suite_teste, nu_seq_caso_teste)
  end

  def insert_registro_teste (nu_seq_suite_caso_teste, test_status, response, ds_error, duration, environment)
    @Helper = Helper.new
    now = Time.now.strftime("%d/%m/%Y %H:%M:%S")

    primeira_parte = "INSERT INTO qa.log_teste VALUES (DEFAULT, "
    segunda_parte = nu_seq_suite_caso_teste + ", '#{test_status}', '" + response + "',"
    terceira_parte = ds_error.nil? ? "''" + ", '" : "'" + ds_error + "',' "
    quarta_parte = now + "', '" + duration.to_s + "', '" + environment + "', '"
    quinta_parte = @Helper.get_current_user_pc + "', '" + @Helper.local_ip + "')"

    query(primeira_parte + segunda_parte + terceira_parte + quarta_parte + quinta_parte)
  end

end