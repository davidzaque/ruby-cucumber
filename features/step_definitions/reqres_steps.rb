require_relative '../../features/helper/response_helper'
Response = ResponseHelper.new

Dado("o endereço da API para manter o cadastro dos usuários do sistema") do
end

Quando("realizar uma requisição passando o ID {int} do Usuário") do |int|
  $response = @reqres.getUser(int)
end

Então("a API irá retornar os dados do Usuário correspondente respondendo o código {int}") do |int|
  Response.setResponse($response)
  expect($response.code).to eq(int)
  expect($response.message).to eq('OK')
  expect($response["data"]["first_name"]).to eq('Janet')
end

Quando("realizar uma requisição para cadastrar um Usuário") do
  $response = @reqres.postUser
end

Então("a API irá retornar os dados do cadastro do Usuário respondendo o código {int}") do |int|
  Response.setResponse($response)
  expect($response.code).to eq(int)
  expect($response.message).to eq('Created')
  expect(Time.parse($response["createdAt"]).getutc.strftime("%d/%m/%Y %H:%M"))
      .to eq(Time.now.utc.strftime("%d/%m/%Y %H:%M"))
end

Quando("realizar uma requisição para alterar um Usuário passando o ID {int}") do |int|
  $response = @reqres.putUser(int)
end

Então("a API irá retornar os dados do Usuário alterado respondendo o código {int}") do |int|
  Response.setResponse($response)
  expect($response.code).to eq(int)
  expect($response.message).to eq('OK')
  expect(Time.parse($response["updatedAt"]).getutc.strftime("%d/%m/%Y %H:%M"))
      .to eq(Time.now.utc.strftime("%d/%m/%Y %H:%M"))
end

Quando("realizar uma requisição para deletar um Usuário passando o ID {int}") do |int|
  $response = @reqres.deleteUser(int)
end

Então("a API deverá responder com o código {int}") do |int|
  Response.setResponse($response)
  expect($response.code).to eq(int)
  expect($response.body.nil?).to eq(true)
end