require_relative '../../features/helper/response_helper'
id_atual = 201

Dado("o endereço de API para manter o cadastro de Startup") do
end

Quando("realizar uma requisição para cadastrar uma Startup") do
  $response = @startup.postStartup
end

Então("a API irá retornar os dados do cadastro da Startup respondendo o código {int}") do |int|
  Response.setResponse($response)
  expect($response.message).to eq('Created')
  expect($response.code).to eq(int)
end

Quando("realizar uma requisição passando o ID da Startup") do
  $response = @startup.getStartup(id_atual)
end

Então("a API irá retornar os dados da Startup correspondente respondendo o código {int}") do |int|
  Response.setResponse($response)
  expect($response.code).to eq(int)
end

Quando("realizar uma requisição para alterar uma Startup") do
  $response = @startup.putStartup(id_atual)
end

Então("a API deverá retornar os dados da Startup alterados respondendo o código {int}") do |int|
  Response.setResponse($response)
  expect($response.code).to eq(int)
end

Quando("realizar uma requisição para excluir uma Startup") do
  $response = @startup.deleteStartup(id_atual)
end

Então("a API deverá retornar os dados da exclusão respondendo o código {int}") do |int|
  Response.setResponse($response)
  expect($response.code).to eq(int)
end
