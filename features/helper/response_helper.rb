class ResponseHelper
  @@response = nil

  def self.response
    return @@response
  end

  def setResponse(response)
    @@response = response
  end

end