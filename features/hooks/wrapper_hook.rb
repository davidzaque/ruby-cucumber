require_relative '../../features/helper/response_helper'

Before do
  @start_time = Time.now ##
end

After do |scenario|
  @Manager = Manager.new
  @end_time = Time.now
  duration = Time.diff(@start_time, @end_time)[:diff]
  response = String(ResponseHelper.response)
  puts response

  res_suite = @Manager.find_suite scenario.feature.name
  res_suite = @Manager.insert_suite scenario.feature.name if res_suite.values == []
  nu_seq_suite = res_suite[0]["nu_seq_suite_teste"]

  res_caso_teste = @Manager.find_caso_teste scenario.name
  res_caso_teste = @Manager.insert_caso_teste scenario.name if res_caso_teste.values == []
  nu_seq_caso_teste = res_caso_teste[0]["nu_seq_caso_teste"]

  res_suite_caso_teste = @Manager.find_suite_caso_teste(nu_seq_suite, nu_seq_caso_teste)
  res_suite_caso_teste = @Manager.insert_suite_caso_teste(nu_seq_suite, nu_seq_caso_teste) if res_suite_caso_teste.values == []
  nu_seq_suite_caso_teste = res_suite_caso_teste[0]["nu_seq_suite_caso_teste"]

  @Manager.insert_registro_teste(nu_seq_suite_caso_teste, scenario.status, response, scenario.exception, duration, "localhost")
end