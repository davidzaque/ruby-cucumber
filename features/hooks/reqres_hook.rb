Before '@reqres' do
  @name = Faker::Artist.name
  @job = Faker::Job.title

  @body = {
      "name": @name,
      "job": @job
  }
  @body = JSON.generate(@body)

  @reqres = Reqres.new(@body)
end